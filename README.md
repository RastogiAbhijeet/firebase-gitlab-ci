# firebase-gitlab-ci

this particular project is for the training purposes only. The aim of this following repository is to provide boilerplate for firebase project and give an insight as to how deployments to the firebase platform can be automated. 

## Pre-requisites 

- It is expected that you have you a node package manager installed on your system.

  If not then, follow the below link to install npm. 

  ` https://www.npmjs.com/get-npm `

- Second you need to install firebase cli inorder to interact with firebase.
  
  `npm install -g firebase-tools`

- Third, We need to have create-react-app.

  `https://create-react-app.dev/docs/getting-started/`

## Steps to creating a firebase project from scratch

- For this particular repository, our major focus would be on the frontend side of thing.
i.e our end goal from this project is that we are able to create a firebase project and deploy a react-app to it. 

- Once we are done with it, we introduce CI/CD into picture in order to make a fluid integration of code into our repository and in the end we should be able to continuous deploy the changes made to our project to our hosted solution.

```
<!-- To login into your firebase account using your google account. -->
Step 1. firebase login
```

Before proceeding I hope you create a new directory and change the context to that directory and then run the below commands. 

```
<!-- Create a new firebase project. -->
Step 2. firebase init

Here you have an option to choose from your existing gcp projects or 
you can create a new project. 

Step 3. create-react-app <name of the project you want to create> --template typescript
```

Now we need to make a change in the firebase.json file. 
Change the value of the public key to <project-name>/build
and delete the public/build folder in the root dir of the firebase project
( you should see a public/build folder when you initialise a firebase project. )

```
Step 4. cd <project folder>

Step 5. npm run build
```

Now lets check whether your build application is being served properly locally or not before we host our application on firebase.
``` & 
# Run the following command from the root dir of the project.

Step 6. firebase serve
```

If everythings goes well, you should be able to head over to the mentioned address in your browser and see our react application running in all its glory.

## Time to host our application 

```
Step 7. firebase deploy
```

Now you should be able to head over to the url, that you see in your console to see your deployed application.

